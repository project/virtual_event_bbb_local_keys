<?php

namespace Drupal\virtual_event_bbb_local_keys\Plugin\BBBKeyPlugin;

use Drupal\virtual_event_bbb\Plugin\BBBKeyPluginBase;
use Drupal\virtual_events\Entity\VirtualEventsEventEntity;
use Drupal\Core\Form\FormStateInterface;
use Drupal\virtual_event_bbb\VirtualEventBBB;
use BigBlueButton\Parameters\GetMeetingInfoParameters;
use BigBlueButton\Parameters\CreateMeetingParameters;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'virtual_event_bbb_local_keys' soruce.
 *
 * @BBBKeyPlugin(
 *   id = "virtual_event_bbb_local_keys",
 *   label = @Translation("Virtual Event BBB local keys")
 * )
 */
class BBBLocalKey extends BBBKeyPluginBase {
  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(FormStateInterface $form_state, ?array $pluginConfigValues) {
    $form = [];
    $form['url'] = [
      '#type' => 'key_select',
      '#title' => t('Bigbluebutton API Endpoint URL'),
      '#maxlength' => 255,
      '#default_value' => isset($pluginConfigValues['url']) ? $pluginConfigValues['url'] : "",
      '#description' => t("Bigbluebutton API Endpoint URL."),
    ];

    $form['secretKey'] = [
      '#type' => 'key_select',
      '#title' => t('Bigbluebutton API Secret Key'),
      '#maxlength' => 255,
      '#default_value' => isset($pluginConfigValues['secretKey']) ? $pluginConfigValues['secretKey'] : "",
      '#description' => t("Bigbluebutton API Secret Key."),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getKeys($pluginConfigValues) {
    $keys = $pluginConfigValues['data']["keys"]["virtual_event_bbb_local_keys"]["settings"];
    $apiUrl = "";
    if($keys['url'] && \Drupal::service('key.repository')->getKey($keys['url'])){
      $apiUrl = \Drupal::service('key.repository')->getKey($keys['url'])->getKeyValue();
    }
    $secretKey = "";
    if($keys['secretKey'] && \Drupal::service('key.repository')->getKey($keys['secretKey'])){
      $secretKey = \Drupal::service('key.repository')->getKey($keys['secretKey'])->getKeyValue();
    }
    return ["url" => $apiUrl, "secretKey" => $secretKey];
  }
}
